const { db } = require("../pgAdaptor");
const { GraphQLObjectType, GraphQLID,GraphQLList,GraphQLNonNull,GraphQLString,GraphQLDate } = require("graphql");
const { PersonType } = require("./types");

const RootQuery = new GraphQLObjectType({
  name: "RootQueryType",
  type: "Query",
  fields: {
    person: {
      type: PersonType,
      args: { id: { type: GraphQLNonNull(GraphQLID) }},
      resolve(parentValue, args) {
        const query = `SELECT * FROM person WHERE id=$1`;
        const values = [args.id];
        return db
        .one(query, values)
        .then(res => res)
        .catch(err => err);
    
      }
    },
    persons: {
      type: GraphQLList(PersonType),
      args: { 
        email: { type: GraphQLString },
        firstname: { type: GraphQLString },
        lastname: { type: GraphQLString }      
      },
      resolve(parentValue, args) {
        let query = `SELECT * FROM person where 1=1 `;
        let values=[];

        if(args.email){
          query = query + ` and email='` + args.email + `'`;
        }

        if(args.firstname){
          query = query + ` and firstname='` + args.firstname + `'`;
        }

        if(args.lastname){
          query = query + ` and lastname='` + args.lastname + `'`;
        }

        return db
          .any(query, values)
          .then(res => {
            //console.log(JSON.stringify(res));
            return res;
          })
          .catch(err => err);

      }
    },
  }
});




exports.query = RootQuery;
