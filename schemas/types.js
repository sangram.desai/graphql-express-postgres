const graphql = require("graphql");
const { GraphQLObjectType, GraphQLString } = graphql;

const PersonType = new GraphQLObjectType({
  name: "Person",
  type: "Query",
  fields: {
    id: { type: GraphQLString },
    firstname: { type: GraphQLString },
    lastname: { type: GraphQLString },
    email: { type: GraphQLString },
    joined: { type: GraphQLString },
    lastloggedin: { type: GraphQLString }
  }
});

exports.PersonType = PersonType;
