const graphql = require("graphql");
const db = require("../pgAdaptor").db;
const { GraphQLObjectType, GraphQLID, GraphQLString, GraphQLBoolean } = graphql;
const { PersonType } = require("./types");

const RootMutation = new GraphQLObjectType({
  name: "RootMutationType",
  type: "Mutation",
  fields: {
    addPerson: {
      type: PersonType,
      args: {
        firstname: { type: GraphQLString },
        lastname: { type: GraphQLString },
        email: { type: GraphQLString }
      },
      resolve(parentValue, args) {
        const query = `insert into person(firstname,lastname,email,joined,lastloggedin) VALUES ($1, $2, $3, current_timestamp,current_timestamp) RETURNING *`;
        const values = [
          args.firstname,
          args.lastname,
          args.email
        ];

        return db
          .one(query, values)
          .then(res => res)
          .catch(err => err);
      }
    },
    editPerson: {
      type: PersonType,
      args: {
        id:{type:GraphQLString},
        firstname: { type: GraphQLString },
        lastname: { type: GraphQLString },
        email: { type: GraphQLString }
      },
      resolve(parentValue, args) {
        const query = `Update person set firstname=$1,lastname=$2,email=$3 where id=$4 RETURNING *`;
        const values = [
          args.firstname,
          args.lastname,
          args.email,
          args.id
        ];

        return db
          .any(query, values)
          .then(res => {
            console.log(JSON.stringify(res));
            return res[0]
          })
          .catch(err => err);
      }
    },
    deletePerson: {
      type: PersonType,
      args: {
        id:{type:GraphQLString},
      },
      resolve(parentValue, args) {
        const query = `Delete from person where id=$1 RETURNING *`;
        const values = [
          args.id
        ];

        return db
          .any(query, values)
          .then(res => {
            //console.log(JSON.stringify(res));
            return res[0]
          })
          .catch(err => err);
      }
    }
  }
});

exports.mutation = RootMutation;